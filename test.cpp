nclude<fstream.h>
#include<stdio.h>
#include<string.h>
#include<iomanip.h>

//***************************************************************
//                   CLASS USED IN PROJECT
//****************************************************************


class book
{
	char bId[6];
	char bname[50];
	char aname[20];
  public:

    void show_book()
    {
        cout<<"\nBook no. : "<<bId;
        cout<<"\nBook Name : ";
        puts(bname);
        cout<<"Author Name : ";
        puts(aname);
    }

    char* retbId()
    {
        return bId;
    }

    void report()
    {
	cout<<bId<<setw(30)<<bname<<setw(30)<<aname<<endl;
	}


};         //class ends here

//***************************************************************
//        global declaration for stream object, object
//****************************************************************

fstream fp,fp1;
book bk;

//***************************************************************
//        function to read specific record from file
//****************************************************************


void display_spb(char n[])
{
    cout<<"\nBOOK DETAILS\n";
    int flag=0;
    fp.open("book.dat",ios::in);
    while(fp.read((char*)&bk,sizeof(book)))
    {
        if(strcmp(bk.retbId(),n)==0)
        {
            bk.show_book();
             flag=1;
        }
    }
    
    fp.close();
    if(flag==0)
        cout<<"\n\nBook does not exist";
}

//***************************************************************
//        function to display Books list
//****************************************************************

void display_allb()
{
    fp.open("book.dat",ios::in);
	
    if(!fp)
    {
        cout<<"ERROR!!! FILE COULD NOT BE OPEN ";
               return;
    }

    cout<<"\n\n\t\tBook LIST\n\n";
    cout<<"=========================================================================\n";
    cout<<"Book Number"<<setw(20)<<"Book Name"<<setw(25)<<"Author\n";
    cout<<"=========================================================================\n";

    while(fp.read((char*)&bk,sizeof(book)))
    {
        bk.report();
    }
         fp.close();
}

//***************************************************************
//        ADMINISTRATOR MENU FUNCTION
//****************************************************************

void admin_menu()
{
    int ch2;
    cout<<"\n\n\t1.DISPLAY ALL BOOKS ";
    cout<<"\n\n\t2.DISPLAY SPECIFIC BOOK ";
    cout<<"\n\n\t03. EXIT";
    cout<<"\n\n\tPlease Enter Your Choice (1-3) ";
    cin>>ch2;
    switch(ch2)
    {
        case 1: display_allb();break;
        case 2: {
                   char num[6];
                   cout<<"\n\n\tPlease Enter The book No. ";
                   cin>>num;
                   display_spb(num);
                   break;
            }
        case 3:exit(0);
            default:cout<<"\a";
       }
       admin_menu();
}

//***************************************************************
//        THE MAIN FUNCTION OF PROGRAM
//****************************************************************


int main()
{
    char ch;
    
	
    do
    {
          cout<<"\n\n\t01. ADMINISTRATOR MENU";
          cout<<"\n\n\t02. EXIT";
          cout<<"\n\n\tPlease Select Your Option (1-2) ";
          cin>>ch;   
          switch(ch)
          {
              case '1':admin_menu();
                 break;
              case '2':exit(0);
              default :cout<<"\a";
        }
        }while(ch!='2');
return 0;
}

